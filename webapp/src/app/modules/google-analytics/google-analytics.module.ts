import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavigationEnd, Router} from "@angular/router";

declare let gtag: Function;

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class GoogleAnalyticsModule {
  constructor(public router: Router){
    this.router.events.subscribe(event => {
        if(event instanceof NavigationEnd){
          gtag('config', 'UA-156460921-1',
            {
              'page_path': event.urlAfterRedirects
            }
          );
        }
      }
    )}
}
