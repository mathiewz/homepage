import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FrostedGlassPanelComponent } from './components/frosted-glass-panel/frosted-glass-panel.component';



@NgModule({
  declarations: [
    FrostedGlassPanelComponent
  ],
  exports: [
    FrostedGlassPanelComponent
  ],
  imports: [
    CommonModule
  ]
})
export class CommonsModule { }
