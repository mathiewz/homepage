import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-frosted-glass-panel',
  templateUrl: './frosted-glass-panel.component.html',
  styleUrls: ['./frosted-glass-panel.component.sass']
})
export class FrostedGlassPanelComponent implements OnInit {

  @Input() danger = false;

  constructor() { }

  ngOnInit(): void {
  }

}
