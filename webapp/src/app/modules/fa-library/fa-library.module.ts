import {NgModule} from '@angular/core';
import {FaIconLibrary} from '@fortawesome/angular-fontawesome';
import {
  faAddressBook,
  faCommentAlt,
  faFlask,
  faHeart,
  faHome,
  faMoon,
  faSeedling,
  faComment,
  faSun
} from '@fortawesome/free-solid-svg-icons';
import {
  faAngular,
  faGitAlt,
  faGithub,
  faGitlab,
  faLinkedinIn,
  faSteam,
  faTwitter
} from '@fortawesome/free-brands-svg-icons';

@NgModule({
  declarations: [],
  imports: []
})
export class FaLibraryModule {
  constructor(library: FaIconLibrary) {
    // Add an icon to the library for convenient access in other components
    library.addIcons(faHome);
    library.addIcons(faGitlab);
    library.addIcons(faGithub);
    library.addIcons(faGitAlt);
    library.addIcons(faAddressBook);
    library.addIcons(faCommentAlt);
    library.addIcons(faHeart);
    library.addIcons(faAngular);
    library.addIcons(faSeedling);
    library.addIcons(faTwitter);
    library.addIcons(faLinkedinIn);
    library.addIcons(faFlask);
    library.addIcons(faSteam);
    library.addIcons(faMoon);
    library.addIcons(faSun);
    library.addIcons(faComment);
  }
}
