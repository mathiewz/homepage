import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Game} from "../models/game.model";
import {GameInfo} from "../models/gameInfo.model";
import {Lang} from "../models/lang.model";

@Injectable({
  providedIn: 'root'
})
export class SteamService {

  constructor(private httpClient: HttpClient) { }

  getAllLang() : Observable<Lang[]>{
    return this.httpClient.get<Lang[]>('steam/lang');
  }

  getAllGames(username: string) : Observable<Game[]>{
    return this.httpClient.get<Game[]>('steam/games', {params : {username : username}});
  }

  getGameInfo(gameId: string, lang: Lang) : Observable<GameInfo>{
    return this.httpClient.get<GameInfo>(`steam/game/${gameId}`, {params : {lang : lang.code}});
  }
}
