import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Chat} from '../models/chat.model';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private httpClient: HttpClient) { }

  getChatbotAnswer(chat: Chat, apiKey: string): Observable<string>{
    return this.httpClient.post('chat', {chat, apiKey}, {responseType: 'text'});
  }

}
