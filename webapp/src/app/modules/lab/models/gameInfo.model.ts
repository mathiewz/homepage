import {Movie} from "./movie.model";

export class GameInfo {

  id: number;
  name: string;
  description: string;
  banner: string;
  screenshots: string[];
  movies: Movie[];

  constructor(data: any) {
    this.id = data.id;
    this.name = data.name;
    this.description = data.description;
    this.banner = data.banner;
    this.screenshots = data.screenshots;
    this.movies = data.movies.map(movie => new Movie(movie));
  }
}
