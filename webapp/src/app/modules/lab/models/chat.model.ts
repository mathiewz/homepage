export const BOT_NAME = '🤖 Chatbot';

export class Message{
  role: string;
  value: string;

  getDisplayName(){
    if (this.role === 'user'){
      return '🙂 You';
    } else if (this.role === 'assistant'){
      return BOT_NAME;
    }
    return this.role;
  }
}

export class Chat{
  messages: Message[] = [];

  addMessage(role: string, value: string){
    const toAdd = new Message();
    toAdd.value = value;
    toAdd.role = role;
    this.messages.push(toAdd);
  }
}
