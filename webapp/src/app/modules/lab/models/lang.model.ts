export class Lang {

  code: string;
  displayText: string;

  constructor(data: any) {
    this.code = data.code;
    this.displayText = data.displayText;
  }
}
