export class Game {

  id: number;
  name: string;
  icon: string;

  constructor(data: any) {
    this.id = data.id;
    this.name = data.name;
    this.icon = data.icon;
  }



}
