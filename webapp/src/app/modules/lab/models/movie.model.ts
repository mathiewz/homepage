export class Movie {

  thumbnail: string;
  url: string;

  constructor(data: any) {
    this.thumbnail = data.thumbnail;
    this.url = data.url;
  }



}
