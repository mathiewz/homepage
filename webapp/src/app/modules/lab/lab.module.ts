import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LabRoutingModule} from './lab-routing.module';
import {LabComponent} from './lab.component';
import {SteamComponent} from './components/steam.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {SteamGameComponent} from './components/steam-game/steam-game.component';
import { ChatbotComponent } from './components/chatbot/chatbot.component';
import {MarkdownModule} from 'ngx-markdown';
import {CommonsModule} from '../commons/commons.module';


@NgModule({
  declarations: [LabComponent, SteamComponent, SteamGameComponent, ChatbotComponent],
    imports: [
        CommonModule,
        LabRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        FontAwesomeModule,
        MarkdownModule,
        CommonsModule
    ]
})
export class LabModule { }
