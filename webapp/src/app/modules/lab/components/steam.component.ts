import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {SteamService} from '../services/steam.service';
import {Game} from '../models/game.model';
import {GameInfo} from '../models/gameInfo.model';
import {AppComponent} from '../../../app.component';
import {Lang} from '../models/lang.model';

@Component({
  selector: 'app-steam',
  templateUrl: './steam.component.html',
  styleUrls: ['./steam.component.sass']
})
export class SteamComponent implements OnInit, OnDestroy {

  isMobile: boolean;

  username: FormControl<string> = new FormControl('');
  loading = false;
  error = false;

  games: Game[];
  selectedGame: GameInfo;

  languages: Lang[];
  selectedLanguage: Lang;

  constructor(
    private steamService: SteamService,
    private appComponent: AppComponent,
  ) { }

  @HostListener('window:resize', ['$event'])
  onResize(_) {
    this.updateSize();
  }

  ngOnDestroy(): void {
    this.appComponent.toogleFullWidth();
  }

  ngOnInit(): void {
    this.updateSize();
    this.appComponent.setTitle('Steam Library Explorer');
    this.appComponent.toogleFullWidth();
    this.steamService.getAllLang().subscribe(
      lang => {
        this.languages = lang;
        this.selectedLanguage = lang[0];
      },
      error => console.error(error)
    );
  }

  private updateSize() {
    this.isMobile = window.innerWidth < 768;
  }

  loadUser() {
    this.games = undefined;
    this.selectedGame = undefined;
    this.error = false;
    this.loading = true;
    console.log(`loading ${this.username.value} library`);
    this.steamService.getAllGames(this.username.value).subscribe(
      games => this.games = games.sort((a, b) => a.name.localeCompare(b.name)),
      error => {
        console.error(error);
        this.error = true;
      }
    ).add(
      () => this.loading = false
    );
  }

  selectGame(game: Game) {
    console.log(`loading game ${game.id} infos`);
    this.steamService.getGameInfo(game.id.toString(), this.selectedLanguage).subscribe(
      infos => this.selectedGame = infos,
      error => console.error(error)
    );
  }

  selectLang(value: Lang) {
    console.log('new lang : ' + value.code);
    this.selectedLanguage = value;
  }

}
