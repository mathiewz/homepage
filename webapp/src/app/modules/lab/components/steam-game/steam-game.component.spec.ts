import {ComponentFixture, TestBed, waitForAsync} from '@angular/core/testing';

import {SteamGameComponent} from './steam-game.component';

describe('SteamGameComponent', () => {
  let component: SteamGameComponent;
  let fixture: ComponentFixture<SteamGameComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SteamGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SteamGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
