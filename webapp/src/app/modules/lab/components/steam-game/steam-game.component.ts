import {Component, Input, OnInit} from '@angular/core';
import {GameInfo} from "../../models/gameInfo.model";

@Component({
  selector: 'app-steam-game',
  templateUrl: './steam-game.component.html',
  styleUrls: ['./steam-game.component.sass']
})
export class SteamGameComponent implements OnInit {

  @Input() game: GameInfo;

  constructor() { }

  ngOnInit(): void {
  }

}
