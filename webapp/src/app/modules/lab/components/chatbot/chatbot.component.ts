import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {BOT_NAME, Chat} from '../../models/chat.model';
import {ChatService} from '../../services/chat.service';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.sass']
})
export class ChatbotComponent implements OnInit {
  apikey: FormControl<string> = new FormControl('');
  prompt: FormControl<string> = new FormControl('');
  loading = false;
  error = false;
  chat: Chat;
  BOT_NAME = BOT_NAME;

  constructor(private chatService: ChatService) { }

  ngOnInit(): void {
    this.chat = new Chat();
  }

  sendChat() {
    this.chat.addMessage('user', this.prompt.value);
    this.loading = true;
    this.error = false;
    this.chatService.getChatbotAnswer(this.chat, this.apikey.value).subscribe(
      res => {
        this.chat.addMessage('assistant', res);
        this.loading = false;
        this.prompt.setValue('');
      }, error => {
        console.log(error);
        this.loading = false;
        this.error = true;
      }
    );
  }
}
