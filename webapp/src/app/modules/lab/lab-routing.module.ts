import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {LabComponent} from './lab.component';
import {SteamComponent} from './components/steam.component';
import {ChatbotComponent} from './components/chatbot/chatbot.component';

const routes: Routes = [
  { path: '', component: LabComponent },
  { path: 'steam', component: SteamComponent},
  { path: 'chat', component: ChatbotComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LabRoutingModule {}
