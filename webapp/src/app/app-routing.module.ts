import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GitlabComponent} from './components/gitlab/gitlab.component';
import {HomeComponent} from './components/home/home.component';
import {HttpClientModule} from '@angular/common/http';
import {GithubComponent} from './components/github/github.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {ContactComponent} from './components/contact/contact.component';
import {BlogComponent} from './components/blog/blog.component';
import {PostComponent} from './components/blog/post/post.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  },
  {
    path: 'gitlab',
    component: GitlabComponent
  },
  {
    path: 'github',
    component: GithubComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  },
  {
    path: 'blog',
    component: BlogComponent
  },
  {
    path: 'blog/:postTitle',
    component: PostComponent
  },
  {
    path: 'lab',
    loadChildren: () => import('./modules/lab/lab.module').then(m => m.LabModule)
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking'
}), HttpClientModule],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
