import {BrowserModule} from '@angular/platform-browser';
import {APP_ID, NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {GitlabComponent} from './components/gitlab/gitlab.component';
import {HomeComponent} from './components/home/home.component';
import {FormsModule} from '@angular/forms';
import {GitCardComponent} from './components/utils/git-card/git-card.component';
import {HttpClient, provideHttpClient, withInterceptors} from '@angular/common/http';
import {apiInterceptor} from './config/api-interceptor.service';
import {NgChartsModule} from 'ng2-charts';
import {HeaderComponent} from './components/layout/header/header.component';
import {WordCloudComponent} from './components/word-cloud/word-cloud.component';
import {SpinnerComponent} from './components/utils/spinner/spinner.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {GithubComponent} from './components/github/github.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {FaLibraryModule} from './modules/fa-library/fa-library.module';
import {ContactComponent} from './components/contact/contact.component';
import {BlogComponent} from './components/blog/blog.component';
import {PostComponent} from './components/blog/post/post.component';
import {FooterComponent} from './components/layout/footer/footer.component';
import {GoogleAnalyticsModule} from './modules/google-analytics/google-analytics.module';
import {CommonsModule} from './modules/commons/commons.module';
import {TagCloudComponent} from 'angular-tag-cloud-module';
import {MarkdownComponent, provideMarkdown} from 'ngx-markdown';
import {baseUrl} from 'marked-base-url';
import {environment} from "../environments/environment";

@NgModule({
  declarations: [
    AppComponent,
    GitlabComponent,
    HomeComponent,
    GitCardComponent,
    HeaderComponent,
    FooterComponent,
    WordCloudComponent,
    SpinnerComponent,
    GithubComponent,
    PageNotFoundComponent,
    ContactComponent,
    BlogComponent,
    PostComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgChartsModule,
    FontAwesomeModule,
    FaLibraryModule,
    GoogleAnalyticsModule,
    CommonsModule,
    TagCloudComponent,
    MarkdownComponent
  ],
  providers: [
    { provide: APP_ID,  useValue: 'serverApp' },
    provideHttpClient(withInterceptors([apiInterceptor])),
    provideMarkdown({
      markedExtensions: [baseUrl(environment.apiUrl)]
    })
  ],
  exports: [
    SpinnerComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
