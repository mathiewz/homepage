import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {GitDTO} from "../models/gitDTO.model";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class GitDaoService {

  constructor(private httpClient: HttpClient) { }

  getGitlabProjects() : Observable<GitDTO> {
    return this.httpClient.get<GitDTO>('projects/gitlab');
  }

  getGithubProjects() : Observable<GitDTO> {
    return this.httpClient.get<GitDTO>('projects/github');
  }
}
