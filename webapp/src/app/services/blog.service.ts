import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {BlogPostMetadata} from "../models/blogPostMetadata.model";

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private httpClient: HttpClient) { }

  getAllPosts() : Observable<BlogPostMetadata[]>{
    return this.httpClient.get<BlogPostMetadata[]>('blog');
  }

  getPostMetadata(postTitle: string) : Observable<BlogPostMetadata> {
    return this.httpClient.get<BlogPostMetadata>(`blog/${postTitle}/metadata`);
  }

  getPost(postTitle: string) : Observable<string> {
    return this.httpClient.get(`blog/${postTitle}`, {responseType: 'text'});
  }
}
