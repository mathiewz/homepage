import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {contactForm} from "../models/contactForm.model";

@Injectable({
  providedIn: 'root'
})
export class MessengerService {

  constructor(private httpClient: HttpClient) { }

  sendMessage(data: contactForm) :Observable<any> {
    return this.httpClient.post('contact', data);
  }
}
