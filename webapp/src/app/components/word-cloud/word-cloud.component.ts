import {Component} from '@angular/core';
import {CloudData, CloudOptions} from 'angular-tag-cloud-module';

@Component({
  selector: 'app-word-cloud',
  templateUrl: './word-cloud.component.html',
  styleUrls: ['./word-cloud.component.sass']
})
export class WordCloudComponent {

  options: CloudOptions;
  cloudData: CloudData[];

  constructor() {
    this.cloudData  = [
      {text: 'Java', weight: 10},
      {text: 'Back-End', weight: 8},
      {text: 'Gitlab', weight: 7},
      {text: 'Junit', weight: 7},
      {text: 'Spring', weight: 9},
      {text: 'Spring boot', weight: 6},
      {text: 'Spring batch', weight: 6},
      {text: 'Git', weight: 6},
      {text: 'Angular', weight: 5},
      {text: 'JEE', weight: 5},
      {text: 'Mockito', weight: 5},
      {text: 'Maven', weight: 5},
      {text: 'Web Development', weight: 5},
      {text: 'Continuous Integration', weight: 5},
      {text: 'SQL', weight: 4},
      {text: 'Sonar', weight: 4},
      {text: 'Quarkus', weight: 4},
      {text: 'Javascript', weight: 4},
      {text: 'HTML', weight: 4},
      {text: 'Libgdx', weight: 4},
      {text: 'CSS', weight: 4},
      {text: 'Bootstrap', weight: 4},
      {text: 'ImGui', weight: 3},
      {text: 'Selenium', weight: 3},
      {text: 'Github', weight: 3},
      {text: 'Shell', weight: 3},
      {text: 'TypeScript', weight: 3},
      {text: 'Docker', weight: 2},
      {text: 'RxJs', weight: 2},
      {text: 'Front-End', weight: 2},
      {text: 'AWS', weight: 1},
      {text: 'Motion Canvas', weight: 1}
    ];

    this.options = {
      width: 1,
      overflow: false
    };
  }
}
