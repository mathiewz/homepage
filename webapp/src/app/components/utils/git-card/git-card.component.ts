import {Component, Input, OnInit} from '@angular/core';
import {Project} from '../../../models/project.model';
import {NgChartsConfiguration} from 'ng2-charts';


const languageColors: {[key: string]: string} = {
  java: '#ad2b4c',
  typescript: '#2b62ad',
  scss: '#cc6699',
  css: '#5a7d94',
  html: '#f07e4d',
  javascript: '#f0db4f',
  python: '#32a852',
  php: '#a966d9',
  apacheconf: '#787878',
  dart: '#9575cd',
};

@Component({
  selector: 'git-card',
  templateUrl: './git-card.component.html',
  styleUrls: ['./git-card.component.sass']
})
export class GitCardComponent implements OnInit {

  @Input()
  project: Project;
  languageChartData: number[];
  colors: string[];
  languageChartLabels: string[];

  constructor() {
  }

  ngOnInit() {
    const languages = this.project.languages;
    const data: number[] = [];
    const colors: string[] = [];
    const labels: string[] = [];

    // tslint:disable-next-line:only-arrow-functions
    Object.keys(languages).forEach(function(key){
      const value = languages[key];
      data.push(value);
      colors.push(languageColors[key.toLowerCase()]);
      labels.push(key);
    });

    this.languageChartLabels = labels;
    this.colors = colors;
    this.languageChartData = data;
  }

}
