import {Component, OnInit} from '@angular/core';
import {GitDaoService} from "../../services/git-dao.service";
import {Project} from "../../models/project.model";
import {AppComponent} from "../../app.component";

@Component({
  selector: 'app-gitlab',
  templateUrl: './gitlab.component.html',
  styleUrls: ['./gitlab.component.sass']
})
export class GitlabComponent implements OnInit {

  personalProjects: Project[];
  contributedProjects: Project[];

  constructor(
    private gitDAO : GitDaoService,
    private appComponent : AppComponent
  ) { }

  ngOnInit() {
    this.gitDAO.getGitlabProjects().subscribe(
      data => {
        this.contributedProjects = data.contributedProjects;
        this.personalProjects = data.personalProjects;
      },
      error => console.error(error)
    )
    this.appComponent.setTitle('Gitlab projects');
  }

}
