import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AppComponent} from "../../app.component";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  ngOnInit(): void {
    this.appComponent.setTitle();
  }

  constructor(
    private router: Router,
    private appComponent: AppComponent
  ) {
  }
}
