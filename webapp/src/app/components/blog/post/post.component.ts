import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BlogService} from '../../../services/blog.service';
import {AppComponent} from '../../../app.component';
import {BlogPostMetadata} from '../../../models/blogPostMetadata.model';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.sass'],
})
export class PostComponent implements OnInit {

  postMetadata: BlogPostMetadata;
  postContent: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private blogService: BlogService,
    private appComponent: AppComponent
  ) { }

  ngOnInit() {
    let postTitle = '';
    this.route.params.subscribe(
      params => postTitle = params.postTitle
    );
    this.blogService.getPost(postTitle).subscribe(
      response => this.postContent = response,
      error => {
        if (error.status === 404){
          void this.router.navigateByUrl('404');
        }
      }
    );
    this.blogService.getPostMetadata(postTitle).subscribe(
      result => {
        this.postMetadata = result;
        this.appComponent.setTitle(this.postMetadata.title);
      },
      error => console.error(error)
    );
  }

}
