import {Component, OnInit} from '@angular/core';
import {BlogService} from "../../services/blog.service";
import {BlogPostMetadata} from "../../models/blogPostMetadata.model";
import {AppComponent} from "../../app.component";

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.sass']
})
export class BlogComponent implements OnInit {

  posts: BlogPostMetadata[];

  constructor(
    private blogService: BlogService,
    private appComponent: AppComponent
  ) {}

  ngOnInit() {
    this.blogService.getAllPosts().subscribe(
      result => this.posts = result,
      error => console.error(error)
    )
    this.appComponent.setTitle('Blog');
  }

}
