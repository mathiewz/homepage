import {Component, OnInit} from '@angular/core';
import {Project} from "../../models/project.model";
import {GitDaoService} from "../../services/git-dao.service";
import {AppComponent} from "../../app.component";

@Component({
  selector: 'app-github',
  templateUrl: './github.component.html',
  styleUrls: ['./github.component.sass']
})
export class GithubComponent implements OnInit {

  personalProjects: Project[];

  constructor(
    private gitDAO : GitDaoService,
    private appComponent : AppComponent
  ) { }

  ngOnInit() {
    this.gitDAO.getGithubProjects().subscribe(
      data => this.personalProjects = data.personalProjects
    )
    this.appComponent.setTitle('Github projects');
  }

}
