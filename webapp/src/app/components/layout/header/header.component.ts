import {Component, OnInit} from '@angular/core';

class SocialLink {
  icon: string;
  link: string;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit{

  socialLink: SocialLink[];

  constructor(
  ) {
    this.socialLink = [
      {link: 'https://twitter.com/Mathiewz_Lucas', icon: 'twitter'},
      {link: 'https://gitlab.com/mathiewz', icon: 'gitlab'},
      {link: 'https://github.com/mathiewz', icon: 'github'},
      {link: 'https://www.linkedin.com/in/mathieu-lucas/', icon: 'linkedin-in'}
    ];
  }

  ngOnInit(): void {
  }

}
