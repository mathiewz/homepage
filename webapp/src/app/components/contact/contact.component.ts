import {Component, OnInit} from '@angular/core';
import {contactForm} from "../../models/contactForm.model";
import {MessengerService} from "../../services/messenger.service";
import {AppComponent} from "../../app.component";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.sass']
})
export class ContactComponent implements OnInit {

  constructor(
    private messengerService : MessengerService,
    private appComponent : AppComponent
  ) { }

  ngOnInit() {
    this.appComponent.setTitle('Contact Me');
  }

  contact(form: any) {
    let data: contactForm = new contactForm(form);
    console.log(data);
    this.messengerService.sendMessage(data).subscribe();
  }
}
