export class BlogPostMetadata {

  title : string;
  subTitle : string;
  date : Date;
  fileLocation : string;

  constructor(data: any) {
    this.title = data.title;
    this.subTitle = data.subTitle;
    this.date = data.date;
    this.fileLocation = data.fileLocation;
  }

}
