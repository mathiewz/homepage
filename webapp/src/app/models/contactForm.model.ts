export class contactForm {

  from: string;
  subject: string;
  message: string;

  constructor(data:any) {
    this.from = data.from;
    this.subject = data.subject;
    this.message = data.message;
  }

}
