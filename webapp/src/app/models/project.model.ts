export class Project {

  name: string;
  languages: Map<string, number>;
  description: string;
  linkToSource: string;
  linkToCentral: string;

  constructor(data: any) {
    this.name = data.name;
    this.languages = data.language;
    this.description = data.description;
    this.linkToSource = data.linkToSource;
    this.linkToCentral = data.linkToCentral;
  }

}
