import {Project} from "./project.model";

export class GitDTO {

  personalProjects: Project[];
  contributedProjects: Project[];

  constructor(data: any){
    data.personalProjects.forEach(project => this.personalProjects.push(new Project(project)));
    data.contributedProjects.forEach(project => this.contributedProjects.push(new Project(project)));
  }
}
