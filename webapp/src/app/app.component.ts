import {ChangeDetectorRef, Component} from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  public constructor(
    private titleService: Title,
    private changeDetectorRef: ChangeDetectorRef,
  ) {}

  fullWidth = false;

  public setTitle(newTitle?: string) {
    if (newTitle){
      this.titleService.setTitle( `${newTitle} - Mathiewz Homepage` );
    } else {
      this.titleService.setTitle('Mathiewz Homepage');
    }
  }

  public toogleFullWidth(){
    this.fullWidth = !this.fullWidth;
    this.changeDetectorRef.detectChanges();
  }
}
