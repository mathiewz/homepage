# Ajoutez un sticky footer à votre application angular avec bootstrap

Sur mon site, je souhaitais ajouter un sticky footer.
De quoi s'agit il ? C'est un footer qui sera affiché en bas de la fenêtre si le contenu de la page ne va pas jusqu'en bas, et qui sinon se placera à la fin de ma page.

Une image vallant 1000 mots :

<div class="col-12 offset-md-2 col-md-8 offset-lg-3 col-lg-6 justify-content-center">

![](https://css-tricks.com/wp-content/uploads/2016/05/sticky-footer-1.svg)

</div>

## Création du composant

tout d'abord, générons le composant de notre footer avec la commande :

```bash
ng generate component footer
```

ensuite, écrivons le code html de notre footer :

```html
<nav class="navbar navbar-light bg-light">
  <span class="navbar-text mx-auto">
    Made with love, angular and bootstrap !
  </span>
</nav>
```

Notre composant est prêt (libre à vous d'afficher ce que vous voulez dans votre footer).
Maintenant, on va l'afficher sur nos pages, en allant modifier le fichier `src/app/app.component.html` pour y ajouter cette ligne 

```html
<app-footer></app-footer>
```

Le footer devrait désormais s'afficher, en revanche il n'est pas collé au bas de l'écran pour les pages sans scroll.

## Let's be sticky

On y est presque, maintenant on va pouvoir aller modifier notre css pour que notre barre devienne un vrai pied de page.

Tout d'abord dans le fichier de style global `styles.sass` (l'extension et la syntaxe peuvent varier selon votre application) :

```sass
html,
body
  height: 100%
```

Ensuite on va rajouter quelques classes.

Tout d'abord dans le fichier `src/app/app.component.html`

```html
<app-footer class="mt-auto"></app-footer>
```

et enfin dans le fichier `src/index.html`

```html
<app-root class="d-flex flex-column h-100"></app-root>
```

Et voilà, on a maintenant un pied de page sticky dans notre application. Celui ci sera correctement affiché sur toutes les pages, quelque soit leur contenu.
