package com.mathiewz.homepage.dto;

import com.mathiewz.homepage.model.git.Project;

import java.util.List;

public class GitDTO {

    private List<Project> personalProjects;
    private List<Project> contributedProjects;

    public void setPersonalProjects(List<Project> personalProjects) {
        this.personalProjects = personalProjects;
    }

    public List<Project> getPersonalProjects() {
        return personalProjects;
    }

    public void setContributedProjects(List<Project> contributedProjects) {
        this.contributedProjects = contributedProjects;
    }

    public List<Project> getContributedProjects() {
        return contributedProjects;
    }
}
