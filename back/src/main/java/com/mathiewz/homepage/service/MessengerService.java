package com.mathiewz.homepage.service;

import com.github.messenger4j.Messenger;
import com.github.messenger4j.exception.MessengerApiException;
import com.github.messenger4j.exception.MessengerIOException;
import com.github.messenger4j.exception.MessengerVerificationException;
import com.github.messenger4j.send.MessagePayload;
import com.github.messenger4j.send.MessagingType;
import com.github.messenger4j.send.message.TextMessage;
import com.mathiewz.homepage.config.MessengerConfig;
import com.mathiewz.homepage.exception.MessengerException;
import jakarta.enterprise.context.ApplicationScoped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.text.MessageFormat.format;

@ApplicationScoped
public class MessengerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessengerService.class);

    private final MessengerConfig messengerConfig;
    private final Messenger messenger;

    public MessengerService(MessengerConfig messengerConfig) {
        this.messengerConfig = messengerConfig;
        LOGGER.info("Starting Messenger service");
        messenger = Messenger.create(
                messengerConfig.token().page().orElse(""),
                messengerConfig.token().app().orElse(""),
                messengerConfig.token().verify().orElse("")
        );
    }

    public boolean checkToken(String mode, String token) {
        LOGGER.info("Checking messenger webhook");
        try {
            messenger.verifyWebhook(mode, token);
            return true;
        } catch (MessengerVerificationException e) {
            LOGGER.error("failed to check token", e);
            return false;
        }
    }

    public void sendMessage(
            String from,
            String subject,
            String message
    ){
        try{
            MessagePayload payload = MessagePayload.create(
                    messengerConfig.recipient().orElse(""),
                    MessagingType.RESPONSE,
                    TextMessage.create(format(MessengerConfig.MESSSAGE_TEMPLATE, from, subject, message))
            );
            LOGGER.info("Sending messenger message: {}", payload.message().toString());
            messenger.send(payload);
        } catch (MessengerIOException | MessengerApiException e) {
            throw new MessengerException(e);
        }
    }
}
