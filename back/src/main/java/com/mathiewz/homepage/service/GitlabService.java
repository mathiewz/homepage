package com.mathiewz.homepage.service;

import com.mathiewz.homepage.dao.GitlabDao;
import com.mathiewz.homepage.model.git.Project;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class GitlabService {

    private final GitlabDao gitlabDao;

    public GitlabService(GitlabDao gitlabDao) {
        this.gitlabDao = gitlabDao;
    }

    public List<Project> getAllProjectsByUser(String user){
        return gitlabDao.getProjectsByUser(user);
    }

    public List<Project> getProjectsById(String... ids){
        return gitlabDao.getProjectsById(ids);
    }

}
