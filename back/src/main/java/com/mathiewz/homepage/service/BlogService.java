package com.mathiewz.homepage.service;

import com.mathiewz.homepage.exception.BlogPostNotFoundException;
import com.mathiewz.homepage.model.blog.BlogMetadata;
import com.mathiewz.homepage.model.blog.BlogPostsEnum;
import jakarta.enterprise.context.ApplicationScoped;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.text.MessageFormat.format;

@ApplicationScoped
public class BlogService {

    public List<BlogMetadata> getMetadatas() {
        return Stream.of(BlogPostsEnum.values())
                .map(BlogPostsEnum::getMetadata)
                .filter(meta -> meta.getDate().isBefore(LocalDate.now().plusDays(1)))
                .sorted(Comparator.comparing(BlogMetadata::getDate).reversed())
                .collect(Collectors.toList());
    }

    public BlogMetadata getPostMetadatas(String markdownFileName) {
        return Stream.of(BlogPostsEnum.values())
                .map(BlogPostsEnum::getMetadata)
                .filter(meta -> meta.getFileLocation().equals(markdownFileName))
                .findFirst().orElseThrow(() -> new BlogPostNotFoundException(markdownFileName));
    }

    public byte[] getPost(String markdownFileName) {
        InputStream in = getClass().getResourceAsStream(format("/blog/{0}.md", markdownFileName));
        if(in == null){
            throw new BlogPostNotFoundException(markdownFileName);
        }
        try {
            return IOUtils.toByteArray(in);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
