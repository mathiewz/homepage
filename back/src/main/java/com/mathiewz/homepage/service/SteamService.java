package com.mathiewz.homepage.service;

import com.mathiewz.homepage.dao.SteamDao;
import com.mathiewz.homepage.model.steam.Game;
import com.mathiewz.homepage.model.steam.GameData;
import com.mathiewz.homepage.model.steam.Language;
import com.mathiewz.homepage.model.steam.SteamLang;
import jakarta.enterprise.context.ApplicationScoped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class SteamService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SteamService.class);

    private final SteamDao steamDao;

    public SteamService(SteamDao steamDao) {
        this.steamDao = steamDao;
    }

    public List<Game> getOwnedGamesByUsername(String username){
        LOGGER.info("Chargement de la bibliothèque steam pour l'utilisateur {}" ,username);
        return steamDao.getOwnedGames(username);
    }

    public GameData getGameDataById(Integer appId, SteamLang lang){
        LOGGER.info("Chargement du jeu {} en {}", appId, lang.getDisplayName());
        return steamDao.getGameInfo(String.valueOf(appId), lang);
    }

    public List<Language> getSupportedLanguages(){
        return Stream.of(SteamLang.values())
                .map(this::buildLanguage)
                .collect(Collectors.toList());
    }

    private Language buildLanguage(SteamLang steamLang) {
        Language lang = new Language();
        lang.setCode(steamLang.name());
        lang.setDisplayText(steamLang.getDisplayName());
        return lang;
    }
}
