package com.mathiewz.homepage.service;

import com.mathiewz.homepage.dao.GithubDao;
import com.mathiewz.homepage.dto.GitDTO;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class GithubService {

    private final GithubDao githubDao;

    public GithubService(GithubDao githubDao) {
        this.githubDao = githubDao;
    }

    public GitDTO getAllProjectsByUser(String username) {
        GitDTO dto = new GitDTO();
        dto.setPersonalProjects(githubDao.getProjectsByUser(username));
        return dto;
    }
}
