package com.mathiewz.homepage.service;

import com.mathiewz.homepage.dao.OpenAiDAO;
import com.mathiewz.homepage.model.chat.Chat;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ChatService {
    private final OpenAiDAO openAiDao;

    public ChatService(OpenAiDAO openAiDao) {
        this.openAiDao = openAiDao;
    }

    public String sendChat(Chat chat, String apiKey) {
        return openAiDao.sendChat(chat, apiKey);
    }
}
