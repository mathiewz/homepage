package com.mathiewz.homepage.config;

import io.smallrye.config.ConfigMapping;

import java.util.Optional;

@ConfigMapping(prefix = "messenger")
public interface MessengerConfig {

    String MESSSAGE_TEMPLATE = """
            Nouveau message

            De
            {0}

            Sujet
            {1}

            Message
            {2}
            """;

    Optional<String> recipient();

    Token token();

    interface Token {
        Optional<String> page();

        Optional<String> app();

        Optional<String> verify();

    }
}
