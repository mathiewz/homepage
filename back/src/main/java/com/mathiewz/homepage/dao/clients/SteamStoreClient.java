package com.mathiewz.homepage.dao.clients;

import com.mathiewz.homepage.model.steam.api.GameInfo;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient(baseUri = "https://store.steampowered.com/api")
public interface SteamStoreClient {


    @GET
    @Path("appdetails")
    GameInfo getAppDetails(
            @QueryParam("appids") String appId,
            @QueryParam("l") String lang
    );

}
