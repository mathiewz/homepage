package com.mathiewz.homepage.dao.clients;

import com.mathiewz.homepage.model.steam.api.Games;
import com.mathiewz.homepage.model.steam.api.UserInfo;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient(baseUri = "https://api.steampowered.com/")
public interface SteamClient {

    @GET
    @Path("ISteamUser/ResolveVanityURL/v1/")
    UserInfo getUserID(
            @QueryParam("key") String key,
            @QueryParam("vanityurl") String username
    );

    @GET
    @Path("IPlayerService/GetOwnedGames/v1")
    Games getGameLibrary(
            @QueryParam("key") String key,
            @QueryParam("steamid") String steamId,
            @QueryParam("include_played_free_games") boolean includeFreeGames,
            @QueryParam("include_appinfo") boolean includeInfos
    );
}
