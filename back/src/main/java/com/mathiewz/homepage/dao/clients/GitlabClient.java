package com.mathiewz.homepage.dao.clients;

import com.mathiewz.homepage.model.git.gitlab.GitlabProject;
import com.mathiewz.homepage.model.git.gitlab.GitlabUser;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import java.util.List;
import java.util.Map;

@RegisterRestClient(baseUri = "https://gitlab.com/api/v4")
public interface GitlabClient {


    @GET
    @Path("users")
    List<GitlabUser> getUserByUsername(@QueryParam("username") String username);

    @GET
    @Path("users/{userId}/projects")
    List<GitlabProject> getProjectsByUser(@PathParam("userId") String userId);

    @GET
    @Path("projects/{projectId}")
    GitlabProject getProjectById(@PathParam("projectId") String projectId);

    @GET
    @Path("projects/{projectId}/languages")
    Map<String, Double> getProjectLanguages(@PathParam("projectId") String projectId);

}
