package com.mathiewz.homepage.dao;

import com.mathiewz.homepage.dao.clients.SteamClient;
import com.mathiewz.homepage.dao.clients.SteamStoreClient;
import com.mathiewz.homepage.model.steam.Game;
import com.mathiewz.homepage.model.steam.GameData;
import com.mathiewz.homepage.model.steam.SteamLang;
import com.mathiewz.homepage.model.steam.api.GameInfo;
import com.mathiewz.homepage.model.steam.api.Games;
import com.mathiewz.homepage.model.steam.mapper.GameMapper;
import jakarta.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class SteamDao {

    private final GameMapper gameMapper;
    private final SteamClient steamClient;
    private final SteamStoreClient steamStoreClient;
    private final String apiKey;

    public SteamDao(
            GameMapper gameMapper,
            @RestClient SteamClient steamClient,
            @RestClient SteamStoreClient steamStoreClient,
            @ConfigProperty(name = "steam.apiKey") Optional<String> apiKey
    ) {
        this.gameMapper = gameMapper;
        this.steamClient = steamClient;
        this.steamStoreClient = steamStoreClient;
        this.apiKey = apiKey.orElse("");
    }

    public String getUserID(String username){
        return steamClient.getUserID(apiKey, username).getResponse().getSteamid();
    }

    public List<Game> getOwnedGames(String username){

        String userID = getUserID(username);
        Games gameLibrary = steamClient.getGameLibrary(apiKey, userID, true, true);
        return gameMapper.getAllGames(gameLibrary);
    }

    public GameData getGameInfo(String appId, SteamLang lang){
        GameInfo infos = steamStoreClient.getAppDetails(appId, lang.getSteamLang());
        return gameMapper.getData(infos);
    }

}
