package com.mathiewz.homepage.dao.clients;

import com.mathiewz.homepage.model.git.github.GithubProject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import java.util.List;
import java.util.Map;

@RegisterRestClient(baseUri = "https://api.github.com")
public interface GithubClient {

    @GET
    @Path("/users/{username}/repos")
    List<GithubProject> findProjectsByUsername(@PathParam("username") String username);

    @GET
    @Path("/repos/{username}/{project}/languages")
    Map<String, Integer> getProjectLanguages(@PathParam("username") String username, @PathParam("project") String project);

}
