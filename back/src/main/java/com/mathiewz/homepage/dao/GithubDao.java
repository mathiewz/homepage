package com.mathiewz.homepage.dao;

import com.mathiewz.homepage.dao.clients.GithubClient;
import com.mathiewz.homepage.model.git.Project;
import com.mathiewz.homepage.model.git.github.GithubProject;
import io.quarkus.cache.CacheResult;
import jakarta.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ApplicationScoped
public class GithubDao {

    private final GithubClient githubClient;

    public GithubDao(@RestClient GithubClient githubClient) {
        this.githubClient = githubClient;
    }

    @CacheResult(cacheName = "github")
    public List<Project> getProjectsByUser(String username) {
        List<GithubProject> response = githubClient.findProjectsByUsername(username);
        return response.stream()
                .map(apiProject -> apiToProject(apiProject, username))
                .collect(Collectors.toList());
    }

    private Project apiToProject(GithubProject apiProject, String username) {
        return new Project(apiProject.getName(), apiProject.getDescription(), getLanguagesByProject(apiProject, username), apiProject.getUrl());
    }

    private Map<String, Double> getLanguagesByProject(GithubProject project, String username){
        Map<String, Integer> languages = githubClient.getProjectLanguages(username, project.getName());
        return languages.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> Double.valueOf(entry.getValue())));
    }
}
