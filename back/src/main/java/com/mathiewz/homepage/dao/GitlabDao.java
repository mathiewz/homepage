package com.mathiewz.homepage.dao;

import com.mathiewz.homepage.dao.clients.GitlabClient;
import com.mathiewz.homepage.model.git.Project;
import com.mathiewz.homepage.model.git.gitlab.GitlabProject;
import com.mathiewz.homepage.model.git.gitlab.GitlabUser;
import io.quarkus.cache.CacheResult;
import jakarta.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
public class GitlabDao {

    private final GitlabClient gitlabClient;

    public GitlabDao(@RestClient GitlabClient gitlabClient) {
        this.gitlabClient = gitlabClient;
    }

    @CacheResult(cacheName = "gitlab")
    public List<Project> getProjectsByUser(String username) {
        GitlabUser user = getUserByUsername(username);
        List<GitlabProject> response = gitlabClient.getProjectsByUser(user.getId());
        return response.stream()
                .map(this::apiToProject)
                .collect(Collectors.toList());
    }

    private GitlabUser getUserByUsername(String username){
        return gitlabClient.getUserByUsername(username).get(0);
    }

    public List<Project> getProjectsById(String... ids) {
        return Stream.of(ids)
                .map(gitlabClient::getProjectById)
                .map(this::apiToProject)
                .collect(Collectors.toList());
    }

    private Project apiToProject(GitlabProject apiProject) {
        return new Project(apiProject.getName(), apiProject.getDescription(), getLanguagesByProject(apiProject), apiProject.getWebUrl());
    }

    private Map<String, Double> getLanguagesByProject(GitlabProject project){
        return gitlabClient.getProjectLanguages(project.getId());
    }
}
