package com.mathiewz.homepage.dao;

import com.mathiewz.homepage.dao.clients.OpenAiClient;
import com.mathiewz.homepage.model.chat.Chat;
import com.mathiewz.homepage.model.chat.api.ChatResponse;
import com.mathiewz.homepage.model.chat.api.Message;
import com.mathiewz.homepage.model.chat.api.Prompt;
import jakarta.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import java.util.stream.Collectors;

@ApplicationScoped
public class OpenAiDAO {

    private static final String GPT_MODEL = "gpt-3.5-turbo";
    private final OpenAiClient openAiClient;

    public OpenAiDAO(@RestClient OpenAiClient openAiClient) {
        this.openAiClient = openAiClient;
    }

    public String sendChat(Chat chat, String apiKey) {
        Prompt request = new Prompt();
        request.setModel(GPT_MODEL);
        request.setMessages(chat.getMessages().stream().map(OpenAiDAO::convertToApiMessage).collect(Collectors.toList()));

        ChatResponse response = openAiClient.getChatCompletion(request, "Bearer "+apiKey);
        return response.getMessage();
    }

    private static Message convertToApiMessage(com.mathiewz.homepage.model.chat.Message message) {
        Message apiMessage = new Message();
        apiMessage.setRole(message.getRole());
        apiMessage.setContent(message.getValue());
        return apiMessage;
    }
}
