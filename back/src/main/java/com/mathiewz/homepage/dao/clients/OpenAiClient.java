package com.mathiewz.homepage.dao.clients;

import com.mathiewz.homepage.model.chat.api.ChatResponse;
import com.mathiewz.homepage.model.chat.api.Prompt;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient(baseUri = "https://api.openai.com/v1")
public interface OpenAiClient {

    @POST
    @Path("chat/completions")
    ChatResponse getChatCompletion(Prompt body, @HeaderParam("Authorization") String bearerToken);
}
