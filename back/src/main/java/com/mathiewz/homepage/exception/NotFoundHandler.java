package com.mathiewz.homepage.exception;

import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class NotFoundHandler implements ExceptionMapper<NotFoundException> {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotFoundHandler.class);
    @Override
    public Response toResponse(NotFoundException e) {
        LOGGER.error("404 not found : {}", e.getLocalizedMessage());
        return Response.status(Response.Status.NOT_FOUND).build();
    }
}
