package com.mathiewz.homepage.exception;

public class MessengerException extends RuntimeException {

    public MessengerException(Throwable cause) {
        super(cause);
    }
}
