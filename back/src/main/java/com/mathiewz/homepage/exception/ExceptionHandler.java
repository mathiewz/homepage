package com.mathiewz.homepage.exception;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import org.jboss.resteasy.reactive.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class ExceptionHandler implements ExceptionMapper<Exception> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandler.class);

    public Response toResponse(Exception e) {
        LOGGER.error("Erreur lors du traitement", e);
        return Response.status(RestResponse.Status.INTERNAL_SERVER_ERROR).build();
    }

}
