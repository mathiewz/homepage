package com.mathiewz.homepage.exception;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class BlogPostNotFoundExceptionHandler implements ExceptionMapper<BlogPostNotFoundException> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BlogPostNotFoundExceptionHandler.class);

    public Response toResponse(BlogPostNotFoundException e) {
        LOGGER.error("Erreur lors de la récupération d'un post : ", e);
        return Response.status(Response.Status.NOT_FOUND).build();
    }


}
