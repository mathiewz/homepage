package com.mathiewz.homepage.exception;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import org.jboss.resteasy.reactive.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class MessengerExceptionHandler implements ExceptionMapper<MessengerException> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessengerExceptionHandler.class);

    public Response toResponse(MessengerException e) {
        LOGGER.error("Erreur lors de l'envoi d'un message au chatbot : ", e.getCause());
        return Response.status(RestResponse.Status.INTERNAL_SERVER_ERROR).build();
    }


}
