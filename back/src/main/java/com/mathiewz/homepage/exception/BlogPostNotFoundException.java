package com.mathiewz.homepage.exception;

import static java.text.MessageFormat.format;

public class BlogPostNotFoundException extends RuntimeException {
    public BlogPostNotFoundException(String markdownFileName) {
        super(format("Markdown file not found : {0}", markdownFileName));
    }
}
