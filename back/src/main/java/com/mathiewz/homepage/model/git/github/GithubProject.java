package com.mathiewz.homepage.model.git.github;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GithubProject {

    private String name;
    private String description;

    @JsonProperty("html_url")
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
