package com.mathiewz.homepage.model.blog;

import java.time.LocalDate;

public class BlogMetadata {

    private final String title;
    private final String subTitle;
    private final LocalDate date;
    private final String fileLocation;

    public BlogMetadata(String title, String subTitle, LocalDate date, String fileLocation) {
        this.title = title;
        this.subTitle = subTitle;
        this.date = date;
        this.fileLocation = fileLocation;
    }

    public String getTitle() {
        return title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getFileLocation() {
        return fileLocation;
    }
}
