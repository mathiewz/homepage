package com.mathiewz.homepage.model.steam.api;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GameInfo {

    public enum Key {
        ID("steam_appid"),
        NAME("name"),
        ABOUT("about_the_game"),
        SCREENSHOTS("screenshots"),
        MOVIES("movies"),
        HEADER("header_image");

        private final String name;

        Key(String name) {
            this.name = name;
        }
    }

    private Map<String, Map<String, Object>> nestedGameInfo = new HashMap<>();
    private String appId;

    @JsonAnySetter
    public void setData(String name, Map<String, Object> value) {
        appId = name;
        nestedGameInfo.put(name, value);
    }

    public <R> R getData(Key key){
        Map<String, Object> data = (Map<String, Object>) nestedGameInfo.get(appId).get("data");
        return (R) data.get(key.name);
    }

    public List<String> getScreenshots(){
        List<Map<String, String>> screenshots = getData(Key.SCREENSHOTS);
        return screenshots.stream()
            .map(map -> map.get("path_full"))
            .collect(Collectors.toList());
    }

}
