package com.mathiewz.homepage.model.steam;

public enum SteamLang {

    EN("English","english"),
    FR("French","french"),
    DE("German","german"),
    ES("Spanish","spanish"),
    IT("Italian","italian"),
    PO("Portuguese","portuguese"),
    JP("Japanese","japanese"),
    RU("Russian","russian"),
    KOR("Korean","korean"),
    POL("Polish","polish");

    private final String displayName;
    private final String steamLang;

    SteamLang(String displayName, String steamLang) {
        this.displayName = displayName;
        this.steamLang = steamLang;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getSteamLang() {
        return steamLang;
    }
}
