package com.mathiewz.homepage.model.git;

import org.apache.commons.lang.StringUtils;

import java.util.Map;

public class Project {

    private final String name;
    private final String description;
    private final Map<String, Double> languages;
    private final String linkToSource;
    private final String linkToCentral;

    public Project(String name, String description, Map<String, Double> languages, String linkToSource) {
        this(name, description, languages, linkToSource, null);
    }

    private Project(String name, String description, Map<String, Double> languages, String linkToSource, String linkToCentral) {
        this.name = name;
        this.description = description;
        this.languages = languages;
        this.linkToSource = linkToSource;
        this.linkToCentral = linkToCentral;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Map<String, Double> getLanguages() {
        return languages;
    }

    public String getLinkToSource() {
        return linkToSource;
    }

    public String getLinkToCentral() {
        return StringUtils.defaultString(linkToCentral);
    }
}
