package com.mathiewz.homepage.model.steam.mapper;

import com.mathiewz.homepage.model.steam.Game;
import com.mathiewz.homepage.model.steam.GameData;
import com.mathiewz.homepage.model.steam.Movie;
import com.mathiewz.homepage.model.steam.api.GameInfo;
import com.mathiewz.homepage.model.steam.api.Games;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.text.MessageFormat.format;

@ApplicationScoped
public class GameMapper {

    private static final String STEAM_MEDIA_URI = "http://media.steampowered.com/steamcommunity/public/images/apps/{0}/{1}.jpg";

    public List<Game> getAllGames(Games games){
        return games.getResponse().getGames().stream()
                .map(this::buildGame)
                .collect(Collectors.toList());
    }

    private Game buildGame(Games.Response.Game game) {
        return new Game(game.getAppId(), game.getName(), getIconURI(game));
    }

    private String getIconURI(Games.Response.Game game){
        return format(STEAM_MEDIA_URI, String.valueOf(game.getAppId()), game.getIcon());
    }

    public GameData getData(GameInfo infos) {
        GameData data = new GameData();
        data.setId(infos.getData(GameInfo.Key.ID));
        data.setName(infos.getData(GameInfo.Key.NAME));
        data.setDescription(infos.getData(GameInfo.Key.ABOUT));
        data.setBanner(infos.getData(GameInfo.Key.HEADER));
        data.setScreenshots(infos.getScreenshots());
        data.setMovies(buildMovies(infos));
        return data;
    }

    public List<Movie> buildMovies(GameInfo infos){
        List<Map<String, Object>> movies = infos.getData(GameInfo.Key.MOVIES);
        if(movies == null){
            return new ArrayList<>();
        }
        return movies.stream()
                .map(this::extractMovie)
                .collect(Collectors.toList());
    }

    private Movie extractMovie(Map<String, Object> map) {
        Movie movie = new Movie();
        movie.setUrl(((Map<String, String>) map.get("webm")).get("max"));
        movie.setThumbnail((String) map.get("thumbnail"));
        return movie;
    }
}
