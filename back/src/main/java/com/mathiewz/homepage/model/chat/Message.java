package com.mathiewz.homepage.model.chat;

public class Message {

    private String role;
    private String value;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
