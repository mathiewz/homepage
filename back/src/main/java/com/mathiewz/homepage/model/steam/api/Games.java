package com.mathiewz.homepage.model.steam.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Games {

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Response {

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class Game {

            @JsonProperty("appid")
            private Integer appId;

            private String name;

            @JsonProperty("img_icon_url")
            private String icon;

            public Integer getAppId() {
                return appId;
            }

            public void setAppId(Integer appId) {
                this.appId = appId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getIcon() {
                return icon;
            }

            public void setIcon(String icon) {
                this.icon = icon;
            }
        }

        private List<Game> games;

        public List<Game> getGames() {
            return games;
        }

        public void setGames(List<Game> games) {
            this.games = games;
        }
    }

    private Response response;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
}
