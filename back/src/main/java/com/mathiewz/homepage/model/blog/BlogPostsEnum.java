package com.mathiewz.homepage.model.blog;

import java.time.LocalDate;
import java.time.Month;

public enum BlogPostsEnum {

    SPINNER(new BlogMetadata(
            "Afficher un spinner lors d'un chargement avec angular",
            "Avertir visuellement vos utilisateurs d'un chargement",
            LocalDate.of(2020, Month.JANUARY, 15),
            "angular-spinner")
    ),

    FOOTER(new BlogMetadata(
            "Ajoutez un sticky footer à votre application angular avec bootstrap",
            "Affichez des infos en bas du navigateur quelque soit le contenu de la page",
            LocalDate.of(2020, Month.JANUARY, 16),
            "footer")
    ),

    CAFFEINE(new BlogMetadata(
            "Mettre en place un cache sur spring avec Caffeine",
            "Accélérez votre application grâce à la mise en cache",
            LocalDate.of(2020, Month.JANUARY, 22),
            "cache-caffeine")
    );


    private final BlogMetadata metadata;

    BlogPostsEnum(BlogMetadata metadata) {
        this.metadata = metadata;
    }

    public BlogMetadata getMetadata() {
        return metadata;
    }
}
