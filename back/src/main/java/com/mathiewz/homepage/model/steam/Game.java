package com.mathiewz.homepage.model.steam;

public class Game {

    private final int id;
    private final String name;
    private final String icon;

    public Game(int id, String name, String icon) {
        this.id = id;
        this.name = name;
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getIcon() {
        return icon;
    }
}
