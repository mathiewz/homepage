package com.mathiewz.homepage.controller;

import com.mathiewz.homepage.dto.GitDTO;
import com.mathiewz.homepage.service.GithubService;
import com.mathiewz.homepage.service.GitlabService;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

@Path("projects")
public class ProjectController {

    private final GitlabService gitlabService;
    private final GithubService githubService;

    private static final String[] CONTRIBUTED_GITLAB_PROJECT_IDS = {"7373767", "5724028"};

    public ProjectController(GitlabService gitlabService, GithubService githubService) {
        this.gitlabService = gitlabService;
        this.githubService = githubService;
    }

    @GET
    @Path("gitlab")
    public GitDTO gitlab(){
        GitDTO gitDTO = new GitDTO();
        gitDTO.setPersonalProjects(gitlabService.getAllProjectsByUser("mathiewz"));
        gitDTO.setContributedProjects(gitlabService.getProjectsById(CONTRIBUTED_GITLAB_PROJECT_IDS));
        return gitDTO;
    }

    @GET
    @Path("github")
    public GitDTO github(){
        return githubService.getAllProjectsByUser("mathiewz");
    }

}
