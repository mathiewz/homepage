package com.mathiewz.homepage.controller;

import com.mathiewz.homepage.model.chat.ChatDTO;
import com.mathiewz.homepage.service.ChatService;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;

@Path("/chat")
public class ChatbotController {

    private final ChatService chatService;

    public ChatbotController(ChatService chatService) {
        this.chatService = chatService;
    }

    @POST
    public String sendMessage(ChatDTO form) {
        return chatService.sendChat(form.getChat(), form.getApiKey());
    }

}
