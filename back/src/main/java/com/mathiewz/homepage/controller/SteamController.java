package com.mathiewz.homepage.controller;

import com.mathiewz.homepage.model.steam.Game;
import com.mathiewz.homepage.model.steam.GameData;
import com.mathiewz.homepage.model.steam.Language;
import com.mathiewz.homepage.model.steam.SteamLang;
import com.mathiewz.homepage.service.SteamService;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;

import java.util.List;

@Path("/steam")
public class SteamController {

    private final SteamService steamService;

    public SteamController(SteamService steamService) {
        this.steamService = steamService;
    }

    @GET
    @Path("/games")
    public List<Game> getUser(@QueryParam("username") String username){
        return steamService.getOwnedGamesByUsername(username);
    }

    @GET
    @Path("/game/{appId}")
    public GameData getUser(
            Integer appId,
            @QueryParam("lang") SteamLang lang
    ){
        return steamService.getGameDataById(appId, lang);
    }

    @GET
    @Path("/lang")
    public List<Language> getSupportedLanguages(){
        return steamService.getSupportedLanguages();
    }

}
