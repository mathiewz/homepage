package com.mathiewz.homepage.controller;

import com.mathiewz.homepage.dto.ContactDTO;
import com.mathiewz.homepage.service.MessengerService;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import org.jboss.resteasy.reactive.RestResponse;

@Path("contact")
public class ContactController {

    private final MessengerService messengerService;

    public ContactController(MessengerService messengerService) {
        this.messengerService = messengerService;
    }

    @GET
    public RestResponse<String> verifyWebhook(
            @QueryParam("hub.mode") String mode,
            @QueryParam("hub.challenge") String challenge,
            @QueryParam("hub.verify_token") String token
    ) {
        if(messengerService.checkToken(mode, token)){
            return RestResponse.ok(challenge);
        }
        return RestResponse.status(RestResponse.Status.FORBIDDEN);
    }

    @POST
    public RestResponse<Void> sendMessage(
            ContactDTO form
    ) {
        messengerService.sendMessage(form.getFrom(), form.getSubject(), form.getMessage());
        return RestResponse.ok();
    }

}
