package com.mathiewz.homepage.controller;

import com.mathiewz.homepage.model.blog.BlogMetadata;
import com.mathiewz.homepage.service.BlogService;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

import java.util.List;

@Path("/blog")
public class BlogController {

    private final BlogService blogService;

    public BlogController(BlogService blogService) {
        this.blogService = blogService;
    }

    @GET
    public List<BlogMetadata> getAllDatas() {
        return blogService.getMetadatas();
    }

    @GET
    @Path("{postTitle}")
    public byte[] getPost(
            @PathParam("postTitle") String markdownFileName
    ) {
        return blogService.getPost(markdownFileName);
    }

    @GET
    @Path("{postTitle}/metadata")
    public BlogMetadata getPostMetadatas(
            @PathParam("postTitle") String markdownFileName
    ) {
        return blogService.getPostMetadatas(markdownFileName);
    }
}
