package com.mathiewz.homepage.service;

import com.mathiewz.homepage.exception.BlogPostNotFoundException;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@QuarkusTest
class BlogServiceTest {

    private BlogService blogService;

    @BeforeEach
    void setup(){
        blogService = new BlogService();
    }

    @Test
    void it_should_return_BlogPostNotFoundException_when_post_does_not_exists(){
        //Given a not existing blog post
        String markdownFileName = "notABlogPost.unknown";

        //When blogService.getPost is called
        //Then it should throw a BlogPostNotFoundException
        Assertions.assertThrows(BlogPostNotFoundException.class, () -> blogService.getPost(markdownFileName));
    }

}