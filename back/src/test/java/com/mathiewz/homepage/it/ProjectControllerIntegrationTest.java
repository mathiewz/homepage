package com.mathiewz.homepage.it;

import com.mathiewz.homepage.dao.clients.GitlabClient;
import com.mathiewz.homepage.model.git.gitlab.GitlabProject;
import com.mathiewz.homepage.model.git.gitlab.GitlabUser;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.path.json.JsonPath;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
public class ProjectControllerIntegrationTest {

    private static final JsonPath GITLAB_RESPONSE = new JsonPath("""
    {"personalProjects":[
        {
            "name":"Project one",
            "description":"Dummy project one",
            "languages":{},
            "linkToSource":"https://exemple.com",
            "linkToCentral":""
        },{
            "name":"Project two",
            "description":"Dummy project two",
            "languages":{},
            "linkToSource":"https://exemple.com",
            "linkToCentral":""
        }
    ],
    "contributedProjects":[
        {
            "name":"Project three",
            "description":"Dummy project three",
            "languages":{},
            "linkToSource":"https://exemple.com",
            "linkToCentral":""
        },{
            "name":"Project four",
            "description":"Dummy project four",
            "languages":{},
            "linkToSource":"https://exemple.com",
            "linkToCentral":""
        }
    ]}
    """);

    @InjectMock
    @RestClient
    private GitlabClient gitlabClient;

    @Test
    public void testHelloEndpoint() {
        GitlabUser user = new GitlabUser();
        String userId = "123";
        user.setId(userId);

        GitlabProject projectOne = new GitlabProject();
        projectOne.setDescription("Dummy project one");
        projectOne.setName("Project one");
        projectOne.setWebUrl("https://exemple.com");

        GitlabProject projectTwo = new GitlabProject();
        projectTwo.setDescription("Dummy project two");
        projectTwo.setName("Project two");
        projectTwo.setWebUrl("https://exemple.com");

        GitlabProject projectThree = new GitlabProject();
        projectThree.setDescription("Dummy project three");
        projectThree.setName("Project three");
        projectThree.setWebUrl("https://exemple.com");

        GitlabProject projectFour = new GitlabProject();
        projectFour.setDescription("Dummy project four");
        projectFour.setName("Project four");
        projectFour.setWebUrl("https://exemple.com");

        Mockito.when(gitlabClient.getUserByUsername("mathiewz")).thenReturn(List.of(user));
        Mockito.when(gitlabClient.getProjectsByUser(userId)).thenReturn(List.of(projectOne, projectTwo));
        Mockito.when(gitlabClient.getProjectById("7373767")).thenReturn(projectThree);
        Mockito.when(gitlabClient.getProjectById("5724028")).thenReturn(projectFour);

        given()
                .when().get("/projects/gitlab")
                .then()
                .statusCode(200)
                .body("", equalTo(GITLAB_RESPONSE.getMap("")));
    }

}
